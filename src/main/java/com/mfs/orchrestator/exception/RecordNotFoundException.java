package com.mfs.orchrestator.exception;

public class RecordNotFoundException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -820165243931188864L;

	private String message;
	public RecordNotFoundException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
		this.message=message;
	}
	

}
