package com.mfs.orchrestator.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mfs.orchrestator.entity.AccHolderInfo;
import com.mfs.orchrestator.repo.AccHolderRepo;
@Service
@Transactional(readOnly = true)
public class AccHolderServiceImpl implements AccHolderService {
	@Autowired
	   private AccHolderRepo repo;
	   @Transactional
	@Override
	public Long save(AccHolderInfo info) {
		   return repo.save(info);
	}

	@Override
	public AccHolderInfo get(Long id) {
		 return repo.get(id);
	   }
	

	@Override
	public List<AccHolderInfo> list() {
		 return repo.list();
	   }
	}

	
