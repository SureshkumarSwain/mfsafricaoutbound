package com.mfs.orchrestator.service;

import java.util.List;

import com.mfs.orchrestator.entity.AccHolderInfo;

public interface AccHolderService {

	Long save(AccHolderInfo info);

	AccHolderInfo get(Long id);

	List<AccHolderInfo> list();

}
