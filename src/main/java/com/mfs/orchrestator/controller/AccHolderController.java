package com.mfs.orchrestator.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.mfs.orchrestator.entity.AccHolderInfo;
import com.mfs.orchrestator.service.AccHolderService;
import com.mfs.orchrestator.utiliy.ResponseUtily;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;

@Validated
@RestController
@RequestMapping("/mfs/orchrestator")
public class AccHolderController {

	@Autowired
	   private AccHolderService service;
	private static final Logger logger=LoggerFactory.getLogger(AccHolderInfo.class);

	  
	   @PostMapping(path="/save", consumes = {"application/json","application/xml"}, 
			   produces = {"application/json","application/xml"})
	   public ResponseEntity<Object> save(@Valid @RequestBody AccHolderInfo info) {
		   logger.debug("inside save().....");
		   logger.debug(info.toString());
	      Long id = service.save(info);
	      return ResponseUtily.generateSuccessResponse(id);
	   }
	   
	   @GetMapping(path = "/info/{id}", produces = {"application/json","application/xml"})
	   public ResponseEntity<Object> get(@NotNull(message = "Enter Account Holder Id.")@PathVariable Long id) {
		   logger.debug("inside holder id"+id);
		   AccHolderInfo info = service.get(id);
	      return ResponseUtily.generateSuccessResponse(info);
	   }

	   
	   @GetMapping(path="/all",produces = {"application/json","application/xml"})
	   public ResponseEntity<Object> listAll() {
		   logger.debug("inside all holder info.....");
	      List<AccHolderInfo> all = service.list();
	      return ResponseUtily.generateSuccessResponse(all);
	   }

}
