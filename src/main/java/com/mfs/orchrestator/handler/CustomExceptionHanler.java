package com.mfs.orchrestator.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;


import com.mfs.orchrestator.exception.RecordNotFoundException;
import com.mfs.orchrestator.utiliy.ResponseUtily;

@RestControllerAdvice
public class CustomExceptionHanler {
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		List<String> details = ex.getConstraintViolations().parallelStream().map(e -> e.getMessage())
				.collect(Collectors.toList());

		return ResponseUtily.generateFailuerResponse(details);
	}

	@ExceptionHandler(RecordNotFoundException.class)
	public final ResponseEntity<Object> handlerRecordNotFoundException(RecordNotFoundException ex, WebRequest request) {

		return ResponseUtily.generateFailuerResponse(ex.getMessage());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			WebRequest request) {
		List<String> details = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())

				.collect(Collectors.toList());

		return ResponseUtily.generateFailuerResponse(details);
	}


	

}
