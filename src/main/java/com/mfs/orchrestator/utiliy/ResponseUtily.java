package com.mfs.orchrestator.utiliy;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mfs.orchrestator.contstants.CommonConstants;



public interface ResponseUtily {

	public static ResponseEntity<Object> generateSuccessResponse(Object responseData){
		Map<String,Object> responseBody=null;
		responseBody=new LinkedHashMap<String, Object>();
		responseBody.put(CommonConstants.MESSAGE,CommonConstants.SUCCESS);
		responseBody.put(CommonConstants.DETAILS,responseData);
		return new ResponseEntity<Object>(responseBody,HttpStatus.OK);
		
	}
	public static ResponseEntity<Object> generateFailuerResponse(Object responseData){
		Map<String,Object> responseBody=null;
		responseBody=new LinkedHashMap<String, Object>();
		responseBody.put(CommonConstants.MESSAGE,CommonConstants.BAD_REQUEST);
		responseBody.put(CommonConstants.DETAILS,responseData);
		return new ResponseEntity<Object>(responseBody,HttpStatus.BAD_REQUEST);
		
	}
}
