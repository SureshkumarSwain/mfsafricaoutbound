package com.mfs.orchrestator.contstants;

public interface CommonConstants {

	String BAD_REQUEST = "Bad Request.";
	String MESSAGE="Message";
	String SUCCESS="Success...";
	String DETAILS="Details";
	String RECORD_NOT_FOUND="No record found !!";
	String INVALID_ID="Invalid AccountHolder Id !!";
}
