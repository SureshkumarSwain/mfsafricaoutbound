package com.mfs.orchrestator.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "account_holder_info")
public class AccHolderInfo  implements Serializable {
	private static final long serialVersionUID = 1L;
	/*
	 * @Id
	 * 
	 * @GenericGenerator(name = "custom-generator", strategy =
	 * "com.mfs.orchrestator.generator.CustomGenerator")
	 * 
	 * @GeneratedValue(generator = "custom-generator")
	 * 
	 * @Column(name = "identity")
	 * 
	 * @SequenceGenerator(name = "gen1", initialValue = 100, allocationSize = 1,
	 * sequenceName = "ac_id")
	 * 
	 * @GeneratedValue(generator = "gen1",strategy = GenerationType.SEQUENCE)
	 */
	
	
	  
	/*
	 * @Id
	 * 
	 * @GenericGenerator(name = "seq_id", strategy =
	 * "com.mfs.orchrestator.generator.CustomGeneratorId")
	 * 
	 * @GeneratedValue(generator = "seq_id")
	 * 
	 * @Column(name = "identity")
	 */
	/*
	 * @Id
	 * 
	 * @GeneratedValue(generator = "uuid")
	 * 
	 * @GenericGenerator(name = "uuid", strategy = "uuid2")
	 * 
	 * @Column(name = "id")
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long identity;
	@Column(name = "first_name")
	@NotEmpty(message = "First name must not be empty")
	private String first_name;
	@Column(name = "middle_name")
	@NotEmpty(message = "middle name must not be empty")
	private String middle_name;
	@Column(name = "surname")
	@NotEmpty(message = "surname must not be empty")
	private String surname;
	@Column(name = "suffix")
	@NotEmpty(message = "surname must not be empty")
	private String suffix;
	@Column(name = "gender")
	@NotEmpty(message = "gender must not be empty")
	private String gender;
	@Column(name = "language")
	@NotEmpty(message = "language must not be empty")
	private String language;
	@Column(name = "date_of_birth")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	// @NotEmpty(message = "date of birth must not be empty")
	private Date date_of_birth;
	@Column(name = "country")
	@NotEmpty(message = "country must not be empty")
	private String country;
	@Column(name = "province")
	@NotEmpty(message = "privince must not be empty")
	private String province;
	@Column(name = "city")
	@NotEmpty(message = "surname must not be empty")
	private String city;
	@Column(name = "occupation")
	@NotEmpty(message = "occupation must not be empty")
	private String occupation;
	@Column(name = "residentialstatus")
	@NotEmpty(message = "residentialstatus must not be empty")
	private String residentialstatus;

}
