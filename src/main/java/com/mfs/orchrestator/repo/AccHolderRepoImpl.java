package com.mfs.orchrestator.repo;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.mfs.orchrestator.entity.AccHolderInfo;
@Repository
public class AccHolderRepoImpl implements  AccHolderRepo{

	   @Autowired
	   private SessionFactory sessionFactory;

	@Override
	public Long save(AccHolderInfo info) {
		sessionFactory.getCurrentSession().save(info);
	      return info.getIdentity();
	}

	@Override
	public AccHolderInfo get(Long id) {
		return sessionFactory.getCurrentSession().get(AccHolderInfo.class, id);
	}

	@Override
	public List<AccHolderInfo> list() {
		 Session session = sessionFactory.getCurrentSession();
	      CriteriaBuilder cb = session.getCriteriaBuilder();
	      CriteriaQuery<AccHolderInfo> cq = cb.createQuery(AccHolderInfo.class);
	      Root<AccHolderInfo> root = cq.from(AccHolderInfo.class);
	      cq.select(root);
	      Query<AccHolderInfo> query = session.createQuery(cq);
	      return query.getResultList();
	}

	

}
