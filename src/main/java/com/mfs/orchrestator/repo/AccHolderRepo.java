package com.mfs.orchrestator.repo;

import java.util.List;

import com.mfs.orchrestator.entity.AccHolderInfo;

public interface AccHolderRepo {
	Long save(AccHolderInfo info);
	AccHolderInfo get(Long id);
	List<AccHolderInfo> list();

}
